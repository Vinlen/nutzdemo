package com.f163.nutz;

import com.f163.nutz.Service.UserService;
import com.f163.nutz.bean.User;
import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.integration.quartz.NutQuartzCronJobFactory;
import org.nutz.ioc.Ioc;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;

public class MainSetup implements Setup {

    // 特别留意一下,是init方法,不是destroy方法!!!!!
    public void init(NutConfig nc) {
        Ioc ioc = nc.getIoc();
        Dao dao = ioc.get(Dao.class);

        Daos.createTablesInPackage(dao, "com.f163.nutz", false);
        Daos.migration(dao, User.class, true, false, false);
        if (dao.count(User.class) == 0) {
            UserService ser = ioc.get(UserService.class);
            ser.add("admin","123456");
        }

        // 获取NutQuartzCronJobFactory从而触发计划任务的初始化与启动
        ioc.get(NutQuartzCronJobFactory.class);



    }

    public void destroy(NutConfig nc) {
    }

}