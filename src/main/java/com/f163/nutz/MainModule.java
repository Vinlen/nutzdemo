package com.f163.nutz;

import org.nutz.integration.shiro.ShiroSessionProvider;
import org.nutz.mvc.annotation.*;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

//默认就是扫描包
@Modules
@Ok("json:full")
@SessionBy(ShiroSessionProvider.class)
@Localization(value = "msg/",defaultLocalizationKey = "zh-CN")
@IocBy(type=ComboIocProvider.class, args={"*js", "ioc/",
        "*anno", "com.f163.nutz",
        "*tx", // 事务拦截 aop
        "*quartz"})
@ChainBy(args = "mvc/nutzbook-mvc-chain.js")
@SetupBy(value = MainSetup.class)
public class MainModule {
}
