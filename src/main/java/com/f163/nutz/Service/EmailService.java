package com.f163.nutz.Service;

public interface EmailService {
    boolean send(String to,String subject,String html);
}
