package com.f163.nutz.Module;


import com.f163.nutz.bean.UserProfile;
import com.f163.nutz.util.Toolkit;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.DaoException;
import org.nutz.dao.FieldFilter;
import org.nutz.dao.util.Daos;
import org.nutz.img.Images;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Logs;
import org.nutz.mvc.Mvcs;
import org.nutz.mvc.Scope;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.*;
import org.nutz.mvc.filter.CheckSession;
import org.nutz.mvc.impl.AdaptorErrorContext;
import org.nutz.mvc.upload.TempFile;
import org.nutz.mvc.upload.UploadAdaptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@IocBean
@At("/user/profile")
public class UserProfileModule extends BaseModule {

    @At
    public UserProfile get(@Attr(scope = Scope.SESSION, value = "me") int userId) {
        UserProfile profile = Daos.ext(dao, FieldFilter.locked(UserProfile.class, "avatar")).fetch(UserProfile.class, userId);
        if (profile == null) {
            profile = new UserProfile();
            profile.setUserId(userId);
            profile.setCreateTime(new Date());
            profile.setUpdateTime(new Date());
            dao.fastInsert(profile);
        }
        return profile;
    }
    @RequiresUser
    @At
    //标记从前端接受的数据是能是json类型
    @AdaptBy(type = JsonAdaptor.class)
    //不做返回
    @Ok("void")
    public void update(@Param("..") UserProfile profile, @Attr(scope = Scope.SESSION, value = "me") int id) {
        if (profile == null)
            return;
        profile.setUserId(id);//修正一下id，防止篡改他们信息
        profile.setUpdateTime(new Date());
        profile.setAvatar(null);//不能通过此方法更新头像
        //获取用户信息，标记为老用户
        UserProfile old = get(id);
        //检查用户邮箱状态
        if (old.getEmail() == null) {
            //如果用户的老邮箱是Null,新的信息表则是uncheck状态
            profile.setEmailChecked(false);
        }
        //如果用户填了老邮箱
        else {
            //如果用户没有填新邮箱，就把老邮箱的信息填给新消息
            if (profile.getEmail() == null) {
                profile.setEmail(old.getEmail());
                profile.setEmailChecked(old.isEmailChecked());
            }
            //或者用户填了新邮箱,但是新旧邮箱信息不相等，则记住未检查
            else if (!profile.getEmail().equals(old.getEmail()))
                profile.setEmailChecked(false);
                //如果两次信息相等，则将旧邮箱的状态给新信息表
            else {
                profile.setEmailChecked(old.isEmailChecked());
            }

        }
        Daos.ext(dao, FieldFilter.create(UserProfile.class, null, "avatar", true)).update(profile);

    }

    @RequiresUser
    //上传头像
    //图片上传 只接受前端传送过来的上传图片
    @AdaptBy(type = UploadAdaptor.class, args = {"${app.root}/WEB-INF/jsp/user_avatar", "8192", "utf-8", "20000", "102400"})
    //要上传图片了  POST请求
    @POST
    //上传成功后 转到profile页
    @Ok(">>:/user/profile")
    @At("/avatar")
    public void uploadAvatar(@Param("file") TempFile file,
                             @Attr(scope = Scope.SESSION, value = "me") int userId,
                             AdaptorErrorContext err) {
        String msg = null;
        if (err != null && err.getAdaptorErr() != null) {
            msg = "图片大小不符合规定";
        } else if (file == null) {
            msg = "空文件";
        } else {
            UserProfile profile = get(userId);

            try {
                BufferedImage image = Images.read(file.getFile());
                image = Images.zoomScale(image, 128, 128, Color.white);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Images.writeJpeg(image, out, 0.8f);
                profile.setAvatar(out.toByteArray());
                dao.update(profile, "^avatar$");
            } catch (DaoException e) {
                Logs.get().info("system error", e);
                msg = "系统错误";
            } catch (Throwable e) {
                msg = "图片格式错误";
            }
        }

        if (msg != null) {
            Mvcs.getHttpSession().setAttribute("upload -error-msg", msg);
        }
    }

    @RequiresUser
    //读取用户头像
    //方法执行成功返回图片
    @Ok("raw:jpg")
    @At("/avatar")
    @GET
    public Object readAvatar(@Attr(scope = Scope.SESSION, value = "me") int userId,
                             HttpServletRequest req) throws SQLException {
        UserProfile profile = Daos.ext(dao, FieldFilter.create(UserProfile.class, "^avatar$")).fetch(UserProfile.class, userId);
        if (profile == null || profile.getAvatar() == null) {

            return new File(req.getServletContext().getRealPath("/rs/user_avatar/fefault.jpg"));

        }
        return profile.getAvatar();
    }

    //内部跳转类
//加/默认 user/profile时执行此方法
    @At("/")
    @GET
    @Ok("jsp:jsp.user.profile")
    public UserProfile index(@Attr(scope = Scope.SESSION, value = "me") int userId) {
        return get(userId);
    }


    @RequiresUser
    @At("/active/mail")
    @POST
    public Object activeMail(@Attr(scope = Scope.SESSION, value = "me") int userId,
                             HttpServletRequest req) {
        NutMap nm = new NutMap();
        UserProfile profile = get(userId);
        if (Strings.isBlank(profile.getEmail())) {
            return nm.setv("ok", false).setv("msg", "未填邮箱");
        }
        //把用户id，用户邮箱，系统时间格式化成一个字符串
        //  String token = String.format("%s,%s,%s", userId, profile.getEmail(), System.currentTimeMillis());
        int arr[] = new int[3];
        arr[0] = userId;
        arr[1] = emailKEY;
        arr[2] = (int) System.currentTimeMillis()/1000;
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
        int token = Toolkit.incode(arr);
        String url = req.getRequestURI() + "?token=" + token;
        String html = "<div>这是你刚才申请的验证邮件<div>如果无法点击,请拷贝一下链接到浏览器中打开<p/>验证链接</div>http://localhost:8080" + url;
        String.format(html, url, url);
        try {
            boolean ok = emailService.send(profile.getEmail(), "这是一封验证邮件", html);
            if (!ok) {
                nm.setv("ok", "false").setv("msg", "发送失败");
            }
        } catch (Throwable e) {
            Logs.get().debug("邮件发送失败", e);
            return nm.setv("ok", "false").setv("msg", "发送失败");
        }
        return nm.setv("ok", true);
    }


    @At("/active/mail")
    @GET
    @Ok("raw") // 为了简单起见,这里直接显示验证结果就好了
    public String activeMailCallback(@Param("token") int token, HttpSession session,
                                     @Attr(scope = Scope.SESSION, value = "me") int userId) {
        int sendTime = Toolkit.outcode(token, userId, emailKEY);
        UserProfile profile = dao.fetch(UserProfile.class, userId);
        int aaa=(int)System.currentTimeMillis()/1000;
        System.out.println("邮件发送时间"+sendTime);
        System.out.println("现在时间："+aaa);
        System.out.println("userid是"+userId);
        System.out.println("邮箱码"+emailKEY);
        //加入 send为666秒  超时时间最大为send+300
        if (aaa>sendTime+300) {
            return "验证信息已超时";
        }
        if (profile.getUserId() == userId) {

            profile.setEmailChecked(true);
            dao.update(profile);
            return "恭喜你于"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"验证邮箱成功";

        }
        return "验证失败，请重新验证";
    }
}

