package com.f163.nutz.Module;

import com.f163.nutz.Service.EmailService;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.lang.random.R;

public  abstract class BaseModule {
  @Inject protected Dao dao;
  @Inject protected EmailService emailService;
  protected int emailKEY= R.random(10000,99999);
}
