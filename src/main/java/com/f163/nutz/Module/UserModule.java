package com.f163.nutz.Module;
import com.f163.nutz.Service.UserService;
import com.f163.nutz.bean.User;
import com.f163.nutz.bean.UserProfile;
import com.f163.nutz.util.Toolkit;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.nutz.aop.interceptor.ioc.TransAop;
import org.nutz.dao.Cnd;
import org.nutz.dao.QueryResult;
import org.nutz.dao.pager.Pager;
import org.nutz.integration.shiro.SimpleShiroToken;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.Scope;
import org.nutz.mvc.annotation.*;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@IocBean
@At("/user")

@Fail("http:500")

public class UserModule extends BaseModule {
     @Inject protected UserService userService;

    @At
    public int count(){
        return dao.count(User.class);
    }

    @At

    public Object login(@Param("username") String name,
                        @Param("password") String password,
                        @Param("captcha") String captcha,
                        @Attr(scope = Scope.SESSION,value = "nutz_captcha") String _captcha,
                        HttpSession session){

        NutMap nm = new NutMap();
        if(!Toolkit.checkCaptcha(captcha,_captcha)){
            return nm.setv("ok",false).setv("msg","验证码错误");
        }
        int userId = userService.fetch(name, password);
       if (userId<0){
           return nm.setv("ok",false).setv("msg","用户名或者密码错误");
       }else {
           session.setAttribute("me",userId);
            SecurityUtils.getSubject().login(new SimpleShiroToken(userId));
           return nm.setv("ok",true);
       }

    }
    @RequiresUser
    @At
    @Ok(">>:")
    public void index(){}


    protected String checkUser(User user, boolean create) {
        if (user == null) {
            return "空对象";
        }
        if (create) {
            if (Strings.isBlank(user.getName()) || Strings.isBlank(user.getPassword()))
                return "用户名/密码不能为空";
        } else {
            if (Strings.isBlank(user.getPassword()))
                return "密码不能为空";
        }
        String passwd = user.getPassword().trim();
        if (6 > passwd.length() || passwd.length() > 12) {
            return "密码长度错误";
        }
        user.setPassword(passwd);
        if (create) {
            int count = dao.count(User.class, Cnd.where("name", "=", user.getName()));
            if (count != 0) {
                return "用户名已经存在";
            }
        } else {
            if (user.getId() < 1) {
                return "用户Id非法";
            }
        }
        if (user.getName() != null)
            user.setName(user.getName().trim());
        return null;
    }






    @At
    @Ok(">>:/")
    public void logout(HttpSession session){
        //清空session域的信息
        session.invalidate();
    }
    @RequiresUser
    @At
    public Object add(@Param("..")User user){
        NutMap nm = new NutMap();
        String msg = checkUser(user, true);
        if(msg!=null){
            return new NutMap().setv("OK","false").setv("msg",msg);
        }
        userService.add(user.getName(),user.getPassword());
        return new NutMap().setv("OK","true").setv("data",user);


    }
    @At("/login")
    @GET
    @Ok("jsp:jsp.user.login")
    public void loginPage(){};
    @RequiresUser
    @At
    public Object update(@Param("password")String password,@Attr("me")int me){
     if(Strings.isBlank(password) || password.length()<6){
         return new NutMap().setv("ok",false).setv("msg","密码不能短于6位");
     }
     userService.updatePassword(me,password);
     return new NutMap().setv("ok",true).setv("msg","密码修改成功");
    }
    @RequiresUser
    @At
    @Aop(TransAop.READ_COMMITTED)
    public Object delete(@Param("id") int id, @Attr("me") int me){
        if(id==me){
            return new NutMap("OK","false").setv("msg","不能删除自己");
        }
        dao.delete(User.class,id);
        dao.clear(UserProfile.class,Cnd.where("userId","=",id));
        return new NutMap("OK","true");

    }
    @RequiresUser
    @At
    public Object query(@Param("name") String name,@Param("..") Pager pager){
        Cnd cnd=Strings.isBlank(name)?null:Cnd.where("name","like","%"+name+"%");
        QueryResult qs = new QueryResult();
        List<User> query = dao.query(User.class, cnd, pager);
        qs.setList(query);
        int count = dao.count(User.class, cnd);
        pager.setRecordCount(count);
        qs.setPager(pager);
        return qs;
    }

}
