package com.f163.nutz.quartz.job;

import com.f163.nutz.bean.User;
import com.f163.nutz.bean.UserProfile;
import com.google.inject.internal.cglib.core.$LocalVariablesSorter;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Sql;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

@IocBean
public class CleanNoActiveUserJob implements Job {
    private static final Log log=Logs.get();
    @Inject
    protected Dao dao;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
       //打印日志，定时任务开始
        log.debug("clean no-active job start");
        Date deadtime = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000L);
        Cnd cnd=Cnd.where("userId",">",10).and("createTime","<",deadtime).and(Cnd.exps("emailChecked","=",false).or("email","IS",null));
        int deleted = dao.clear(UserProfile.class, cnd);
        log.debugf("delete %d UserProfile",deleted);

        Sql sql = Sqls.create("delete from $user_table where id > 10 and not exists (select 1 from $user_profile_table where $user_table.id = uid ) and ct < @deadtime");
        sql.vars().set("user_table", dao.getEntity(User.class).getTableName());
        sql.vars().set("user_profile_table", dao.getEntity(UserProfile.class).getTableName());
        sql.params().set("deadtime", deadtime);
       dao.execute(sql);
       log.debugf("delete %d User",sql.getUpdateCount());

       log.debug("clean no-active User job, Done");
    }
}
